﻿using System.Data.Entity.ModelConfiguration;
using ProjetoModeloDDD.Domain.Entities;
namespace ProjetoModeloDDD.Infra.Data.EntityConfig
{
    public class ClienteConfiguration : EntityTypeConfiguration<Cliente>
    {
        public ClienteConfiguration()
        {
            HasKey(k => k.ClienteId);
            Property(p => p.Nome)
                .IsRequired()
                .HasMaxLength(150);

            Property(p => p.Sobrenome)
               .IsRequired()
               .HasMaxLength(150);

            Property(p => p.Email)
               .IsRequired();
        }
    }
}
